/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      backgroundImage: {
        'paint-img': "url('/images/turquoise-paint.png')",
      },
      colors: {
        primary: '#06959d',
      },
    },
  },
  plugins: [],
}
