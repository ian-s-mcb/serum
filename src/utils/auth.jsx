import { createContext, useContext, useEffect, useState } from 'react'
import { Navigate, useLocation } from 'react-router-dom'

import { supabase } from './supabaseClient'

const AuthContext = createContext(null)

export function AuthProvider({ children }) {
  const [session, setSession] = useState(null)
  const [specifiedUser, setSpecifiedUser] = useState('')

  async function initializeAuthProvider() {
    // Retreive auth info from Supabase API and store in provider
    const {
      data: { session },
    } = await supabase.auth.getSession()
    setSession(session)

    // Set callback for auth events
    supabase.auth.onAuthStateChange((e, session) => {
      if (e === 'SIGNED_OUT') {
        setSession(null)
      } else if (e === 'SIGNED_IN') {
        setSession(session)
      }
    })
  }
  useEffect(() => {
    initializeAuthProvider()
  }, [])

  function specifyUser(newUser) {
    setSpecifiedUser(newUser)
  }
  async function loginWithEmailLink() {
    const data = await supabase.auth.signInWithOtp({
      email: specifiedUser,
    })
    return data
  }
  async function loginWithPassword(password) {
    const data = await supabase.auth.signInWithPassword({
      email: specifiedUser,
      password,
    })
    return data
  }
  async function logout() {
    const data = await supabase.auth.signOut()
    setSession(null)
    return data
  }
  async function signUp(user, password) {
    const data = await supabase.auth.signUp({
      email: user,
      password,
    })
    return data
  }
  const value = {
    loginWithEmailLink,
    loginWithPassword,
    logout,
    user: !session ? null : session.user,
    signUp,
    specifiedUser,
    specifyUser,
  }

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

export function useAuth() {
  return useContext(AuthContext)
}

export function RequireAuth({ children }) {
  const auth = useAuth()
  const location = useLocation()

  return auth.user ? (
    children
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  )
}
