import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from 'react-router-dom'

import App from './components/App'
import { AuthProvider } from './utils/auth'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AuthProvider>
      <RouterProvider
        router={createBrowserRouter(createRoutesFromElements(App))}
      />
    </AuthProvider>
  </React.StrictMode>
)
