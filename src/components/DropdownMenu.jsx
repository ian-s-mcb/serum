import React from 'react'
import { Link } from 'react-router-dom'
import { Menu, Transition } from '@headlessui/react'
import {
  Home as HomeIcon,
  LogIn as LoginIcon,
  LogOut as LogoutIcon,
  Menu as MenuIcon,
  User as UserIcon,
} from 'react-feather'

import { useAuth } from '../utils/auth'

export default function Dropdown() {
  const auth = useAuth()

  async function handleLogoutClick() {
    const { error } = await auth.logout()
    if (error) {
      console.error(error.message)
    }
  }

  return (
    <Menu>
      <Menu.Button>
        <MenuIcon className="dark:text-white h-10 w-10" strokeWidth={3} />
      </Menu.Button>
      <Transition
        as={React.Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute bg-white dark:bg-neutral-700 dark:shadow-none dark:text-white divide-gray-400 divide-y right-0 rounded-md shadow-2xl w-32">
          <div>
            <Menu.Item>
              {({ active }) => (
                <Link
                  className={`${
                    active && 'bg-gray-100 dark:bg-gray-700'
                  } flex items-center pl-2 py-1 rounded-tl-md rounded-tr-md w-full`}
                  to="/"
                >
                  <HomeIcon className="h-5 mr-2 w-5" />
                  Home
                </Link>
              )}
            </Menu.Item>
            {auth.user && (
              <Menu.Item>
                {({ active }) => (
                  <Link
                    className={`${
                      active && 'bg-gray-100 dark:bg-gray-700'
                    } flex items-center pl-2 py-1 w-full`}
                    to="/profile"
                  >
                    <UserIcon className="h-5 mr-2 w-5" />
                    Profile
                  </Link>
                )}
              </Menu.Item>
            )}
          </div>
          <div>
            {auth.user ? (
              <Menu.Item>
                {({ active }) => (
                  <button
                    className={`${
                      active && 'bg-gray-100 dark:bg-gray-700'
                    } flex items-center pl-2 py-1 rounded-bl-md rounded-br-md w-full`}
                    onClick={handleLogoutClick}
                  >
                    <LogoutIcon className="h-5 mr-2 w-5" />
                    Logout
                  </button>
                )}
              </Menu.Item>
            ) : (
              <Menu.Item>
                {({ active }) => (
                  <Link
                    className={`${
                      active && 'bg-gray-100 dark:bg-gray-700'
                    } flex items-center pl-2 py-1 rounded-bl-md rounded-br-md w-full`}
                    to="/login"
                  >
                    <LoginIcon className="h-5 mr-2 w-5" />
                    Login
                  </Link>
                )}
              </Menu.Item>
            )}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  )
}
