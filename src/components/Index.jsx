import { Link } from 'react-router-dom'

export default function Index() {
  return (
    <div className="max-w-4xl mx-auto px-1 space-y-8">
      <div className="md:grid md:grid-cols-3 items-end mt-4">
        <div className="col-span-2">
          <p className="text-3xl text-standout">One injection.</p>
          <p className="text-3xl text-primary">Unimaginable growth.</p>
          <p className="mt-4 text-xl text-standout">
            Accelerate your fledgling web application with SERUM.
          </p>
        </div>
        <div className="flex mt-2">
          <Link className="btn-primary" to="/login">
            Explore its
            <br />
            features
          </Link>
          <a
            className="btn-primary-outline ml-3"
            href="https://gitlab.com/ian-s-mcb/serum"
            rel="noreferrer"
            target="_blank"
          >
            Study its
            <br />
            formula
          </a>
        </div>
      </div>
      <div className="text-normal">
        Every dose contains a
        <ul className="list-disc list-inside">
          <li>A generous scoop of auth</li>
          <li>A spark of reactivity</li>
          <li>A pinch of style</li>
          <li>One large heap of smooth, pre-baked DX</li>
        </ul>
      </div>
      <p className="text-normal">
        Metaphors aside, Supabase Example with React and User Management (SERUM)
        is reference example of how to use the instant API platform named
        Supabase to implement auth.
      </p>
      <div className="text-normal">
        Special thanks to the following projects without which SERUM wouldn’t
        exist.
        <ul className="list-disc list-inside">
          <li>Supabase</li>
          <li>React</li>
          <li>React-Router</li>
          <li>Vite</li>
          <li>Tailwind CSS</li>
          <li>Headless UI</li>
        </ul>
      </div>
    </div>
  )
}
