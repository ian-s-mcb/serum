import { useEffect, useState } from 'react'

import { useAuth } from '../utils/auth'
import { supabase } from '../utils/supabaseClient'
import Avatar from './Avatar'

export default function Profile() {
  const auth = useAuth()
  const [avatarUrl, setAvatarUrl] = useState('')
  const [loading, setLoading] = useState(false)
  const [name, setName] = useState('')
  const [website, setWebsite] = useState('')

  async function getProfile() {
    try {
      if (!auth.user) {
        throw new Error('User not found')
      }
      setLoading(true)
      const { data, error } = await supabase
        .from('profiles')
        .select(`avatar_url, username, website`)
        .eq('id', auth.user.id)
        .single()

      if (error) {
        throw error
      }

      setAvatarUrl(data.avatar_url)
      setName(data.username)
      setWebsite(data.website)
    } catch (error) {
      console.warn(error.message)
    } finally {
      setLoading(false)
    }
  }
  function handleNameChange(e) {
    const newName = e.target.value
    setName(newName)
  }
  async function handleUpdateProfileClick({ avatarUrl, name, website }) {
    try {
      setLoading(true)
      if (!auth.user) {
        throw new Error('User not found')
      }
      const updates = {
        avatar_url: avatarUrl,
        username: name,
        website,
        id: auth.user.id,
        updated_at: new Date(),
      }
      const { error } = await supabase.from('profiles').upsert(updates)

      if (error) {
        throw error
      }
    } catch (error) {
      console.warn(error)
    } finally {
      setLoading(false)
    }
  }
  function handleWebsiteChange(e) {
    const newWebsite = e.target.value
    setWebsite(newWebsite)
  }

  useEffect(() => {
    getProfile()
  }, [])

  return (
    <div className="dark:text-white mx-auto px-1 w-96">
      <h1 className="text-2xl text-center">Profile</h1>
      <div className="mt-4 space-y-2">
        <div className="grid grid-cols-4 items-center">
          <label className="" htmlFor="avatar">
            Avatar
          </label>
          <div className="col-span-3">
            <Avatar
              url={avatarUrl}
              size={150}
              onUpload={(url) => {
                setAvatarUrl(url)
                handleUpdateProfileClick({ avatarUrl: url, name, website })
              }}
            />
          </div>
        </div>
        <div className="grid grid-cols-4 items-center">
          <label className="" htmlFor="email">
            Email
          </label>
          <input
            className="dark:bg-transparent col-span-3 p-2"
            id="email"
            type="text"
            disabled
            value={auth.user ? auth.user.email : ''}
          />
        </div>
        <div className="grid grid-cols-4 items-center">
          <label className="" htmlFor="name">
            Name
          </label>
          <input
            className="col-span-3 text-input"
            id="name"
            onChange={handleNameChange}
            type="text"
            value={name}
          />
        </div>
        <div className="grid grid-cols-4 items-center">
          <label className="" htmlFor="website">
            Website
          </label>
          <input
            className="col-span-3 text-input"
            id="website"
            onChange={handleWebsiteChange}
            type="text"
            value={website}
          />
        </div>
        <div className="grid grid-cols-4 items-center">
          <div className=""></div>
          <div className="col-span-3">
            <button
              className="btn-primary"
              onClick={() =>
                handleUpdateProfileClick({ avatarUrl, name, website })
              }
              disabled={loading}
            >
              {loading ? 'Loading' : 'Update Profile'}
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
