export default function Footer() {
  return (
    <footer className="bg-gray-200 dark:bg-neutral-700 dark:text-gray-400 mt-12 px-1">
      <div className="max-w-4xl mx-auto py-20 text-center">
        <p>
          Copyright @2022{' '}
          <a className="link" href="https://iansmcb.ml">
            Ian S. McBride
          </a>
          . This work is licensed under the{' '}
          <a
            className="link"
            href="https://gitlab.com/ian-s-mcb/serum/-/blob/main/LICENSE"
          >
            MIT License
          </a>
          .
        </p>
        <p>
          See{' '}
          <a className="link" href="https://gitlab.com/ian-s-mcb/serum">
            GitLab
          </a>{' '}
          for source code.
        </p>
        <p>Proudly hosted on Supabase and Netlify.</p>
      </div>
    </footer>
  )
}
