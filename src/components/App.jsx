import { Outlet, Route, useLocation } from 'react-router-dom'

import ErrorMessage from './ErrorMessage'
import Footer from './Footer'
import Header from './Header'
import Index from './Index'
import Login from './Login'
import Profile from './Profile'
import { RequireAuth } from '../utils/auth'

import './App.css'

function Layout() {
  const location = useLocation()
  return (
    <div>
      {location.pathname !== '/login' && <Header />}
      <Outlet />
      {location.pathname !== '/login' && <Footer />}
    </div>
  )
}

const routes = (
  <Route path="/" element={<Layout />} errorElement={<ErrorMessage />}>
    <Route index={true} element={<Index />} />
    <Route path="/login" element={<Login />} />
    <Route
      path="/profile"
      element={
        <RequireAuth>
          <Profile />
        </RequireAuth>
      }
    />
  </Route>
)

export default routes
