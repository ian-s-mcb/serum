import { useState } from 'react'
import { Link, Navigate } from 'react-router-dom'

import Brand from './Brand'
import { useAuth } from '../utils/auth'

function AuthMethodPrompt({
  feedback,
  handleEmailLinkClick,
  handleLoginSubmit,
  handlePasswordChange,
  loadingEmailLink,
  loadingLogin,
  password,
  resetFormAndChooseLogin,
  resetFormAndChooseSignUp,
  specifiedUser,
}) {
  return (
    <>
      <p className="mt-1">
        {specifiedUser}
        <br />
        Not you{'? '}
        <button className="text-primary" onClick={resetFormAndChooseLogin}>
          Switch account
        </button>
      </p>
      <button
        className="btn-primary mt-10"
        onClick={handleEmailLinkClick}
        type="button"
      >
        {loadingEmailLink ? 'Loading' : 'Email me a login link'}
      </button>
      <div className="flex items-center my-6">
        <div className="border-t border-gray-400 flex-grow"></div>
        <span className="flex-shrink mx-4 text-white">Or</span>
        <div className="border-t border-gray-400 flex-grow"></div>
      </div>
      <input
        className="text-input w-full"
        name="password"
        onChange={handlePasswordChange}
        placeholder="Type password"
        type="password"
        value={password}
      />
      <p className={`mt-2 text-left ${feedback.error && 'text-error'}`}>
        {feedback.message}
      </p>
      <div className="flex justify-between mt-4">
        <button
          className="btn-primary-outline"
          onClick={handleLoginSubmit}
          type="submit"
        >
          {loadingLogin ? 'Loading' : 'Continue'}
        </button>
        <button
          className="order-first text-primary"
          onClick={resetFormAndChooseSignUp}
          type="button"
        >
          Sign up instead
        </button>
      </div>
    </>
  )
}

export default function Login() {
  const auth = useAuth()
  const [feedback, setFeedback] = useState({ error: false, message: '' })
  const [loadingEmailLink, setLoadingEmailLink] = useState(false)
  const [loadingLogin, setLoadingLogin] = useState(false)
  const [loadingSignUp, setLoadingSignUp] = useState(false)
  const [newUser, setNewUser] = useState('')
  const [password, setPassword] = useState('')
  const [signUpChosen, setSignUpChosen] = useState(false)

  function clearCredentials() {
    setNewUser('')
    setPassword('')
  }
  function handleEmailChange(e) {
    const newUser = e.target.value
    setNewUser(newUser)
  }
  async function handleEmailLinkClick() {
    // Validate
    if (!auth.specifiedUser) {
      setFeedback({ error: true, message: 'Email required' })
      return
    }

    setLoadingEmailLink(true)
    const { error } = await auth.loginWithEmailLink(password)
    setLoadingEmailLink(false)

    if (error) {
      setFeedback({ error: true, message: error.message })
    } else {
      setFeedback({ error: false, message: 'Email sent' })
    }
    resetForm()
  }
  async function handleLoginSubmit() {
    // Validate
    if (!password || !auth.specifiedUser) {
      setFeedback({ error: true, message: 'Email and password required' })
      return
    }

    setLoadingLogin(true)
    setFeedback({ error: false, message: '' })
    const { error } = await auth.loginWithPassword(password)
    setLoadingLogin(false)

    if (error) {
      setFeedback({ error: true, message: error.message })
      clearCredentials()
    } else {
      resetForm()
    }
  }
  function handlePasswordChange(e) {
    const newPassword = e.target.value
    setPassword(newPassword)
  }
  async function handleSignUpSubmit() {
    // Validate
    if (!password || !newUser) {
      setFeedback({ error: true, message: 'Email and password required' })
      return
    }

    setLoadingSignUp(true)
    setFeedback({ error: false, message: '' })
    const { error } = await auth.signUp(newUser, password)
    setLoadingSignUp(false)

    if (error) {
      setFeedback({ error: true, message: error.message })
    } else {
      setFeedback({
        error: false,
        message: 'Check you email. Email must be validated before first login.',
      })
    }
    resetForm()
  }
  function handleSpecifyEmailSubmit() {
    // Validate
    if (!newUser) {
      setFeedback({ error: true, message: 'Email required' })
      return
    }

    auth.specifyUser(newUser)
    setFeedback({ error: false, message: '' })
  }
  function resetForm() {
    clearCredentials()
    auth.specifyUser('')
  }
  function resetFormAndChooseLogin() {
    resetForm()
    setSignUpChosen(false)
  }
  function resetFormAndChooseSignUp() {
    resetForm()
    setSignUpChosen(true)
  }

  return auth.user ? (
    <Navigate to="/profile" />
  ) : (
    <form
      className="dark:border-gray-700 dark:text-white md:border-2 border-gray-400 h-240 md:mt-20 mx-auto p-5 rounded-lg w-96"
      onSubmit={(e) => e.preventDefault()}
    >
      <Link to="/">
        <Brand className="mt-8" fgColor="black" size="lg" />
      </Link>
      <h3 className="dark:text-white mt-1 text-2xl text-center">
        {signUpChosen ? 'Sign Up' : 'Login'}
      </h3>
      <div className="mx-auto text-center w-72">
        {signUpChosen ? (
          <SignUpPrompt
            {...{
              resetFormAndChooseLogin,
              feedback,
              loadingSignUp,
              newUser,
              password,
              handleEmailChange,
              handlePasswordChange,
              handleSignUpSubmit,
            }}
          />
        ) : auth.specifiedUser ? (
          <AuthMethodPrompt
            {...{
              resetFormAndChooseLogin,
              resetFormAndChooseSignUp,
              feedback,
              handleEmailLinkClick,
              handleLoginSubmit,
              handlePasswordChange,
              loadingEmailLink,
              loadingLogin,
              password,
              specifiedUser: auth.specifiedUser,
            }}
          />
        ) : (
          <SpecifyUserPrompt
            {...{
              feedback,
              handleEmailChange,
              handleSpecifyEmailSubmit,
              newUser,
              setSignUpChosen,
            }}
          />
        )}
      </div>
    </form>
  )
}

function SignUpPrompt({
  resetFormAndChooseLogin,
  feedback,
  loadingSignUp,
  newUser,
  password,
  handleEmailChange,
  handlePasswordChange,
  handleSignUpSubmit,
}) {
  return (
    <>
      <input
        className="mt-8 text-input w-full"
        name="user"
        onChange={handleEmailChange}
        placeholder="Type email"
        required
        type="text"
        value={newUser}
      />
      <input
        className="mt-2 text-input w-full"
        name="password"
        onChange={handlePasswordChange}
        placeholder="Type password"
        required
        type="password"
        value={password}
      />
      <p className={`mt-2 text-left ${feedback.error && 'text-error'}`}>
        {feedback.message}
      </p>
      <div className="flex justify-between mt-12">
        <button
          className="btn-primary"
          onClick={() => handleSignUpSubmit()}
          type="submit"
        >
          {loadingSignUp ? 'Loading' : 'Create'}
        </button>
        <button
          className="order-first text-primary"
          onClick={resetFormAndChooseLogin}
          type="button"
        >
          Login instead
        </button>
      </div>
    </>
  )
}

function SpecifyUserPrompt({
  feedback,
  handleEmailChange,
  handleSpecifyEmailSubmit,
  newUser,
  setSignUpChosen,
}) {
  return (
    <>
      <input
        className="mt-8 text-input w-full"
        inputMode="email"
        autoFocus
        name="user"
        onChange={handleEmailChange}
        placeholder="Enter email"
        required
        maxLength={64}
        type="email"
        value={newUser}
      />
      <p className={`mt-2 text-left ${feedback.error && 'text-error'}`}>
        {feedback.message}
      </p>
      <div className="flex justify-between mt-12">
        <button
          className="btn-primary"
          onClick={() => handleSpecifyEmailSubmit(newUser)}
          type="submit"
        >
          Continue
        </button>
        <button
          className="order-first text-primary"
          onClick={() => setSignUpChosen(true)}
          type="button"
        >
          Create account
        </button>
      </div>
    </>
  )
}
