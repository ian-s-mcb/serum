import { NavLink } from 'react-router-dom'

import Brand from './Brand'
import DropdownMenu from './DropdownMenu'

export default function Header() {
  return (
    <header>
      <div className="flex items-center justify-between max-w-4xl mx-auto px-1 py-2 relative">
        <NavLink to="/">
          <Brand size="md" />
        </NavLink>
        <div>
          <DropdownMenu />
        </div>
      </div>
    </header>
  )
}
