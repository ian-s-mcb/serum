import { useEffect, useState } from 'react'
import { User as UserIcon } from 'react-feather'

import { supabase } from '../utils/supabaseClient'

export default function Avatar({ url, size, onUpload }) {
  const [avatarUrl, setAvatarUrl] = useState(null)
  const [uploading, setUploading] = useState(false)

  async function downloadImage(path) {
    try {
      const { data, error } = await supabase.storage
        .from('avatars')
        .download(path)
      if (error) {
        throw error
      }
      const url = URL.createObjectURL(data)
      setAvatarUrl(url)
    } catch (error) {
      console.warn('Error downloading image: ', error.message)
    }
  }
  async function uploadAvatar(event) {
    try {
      setUploading(true)

      if (!event.target.files || event.target.files.length === 0) {
        throw new Error('You must select an image to upload.')
      }

      const file = event.target.files[0]
      const fileExt = file.name.split('.').pop()
      const fileName = `${Math.random()}.${fileExt}`

      const { error } = await supabase.storage
        .from('avatars')
        .upload(fileName, file)
      if (error) {
        throw error
      }

      onUpload(fileName)
    } catch (error) {
      alert(error.message)
    } finally {
      setUploading(false)
    }
  }
  useEffect(() => {
    if (url) {
      downloadImage(url)
    }
  }, [url])

  return (
    <div>
      {avatarUrl ? (
        <img
          alt="Avatar"
          className="rounded-lg"
          src={avatarUrl}
          style={{ height: size, width: size }}
        />
      ) : (
        <div className="border-2 border-black dark:border-white h-32 p-2 rounded-full w-32">
          <UserIcon className="h-full stroke-1 w-full" />
        </div>
      )}
      <div className="mt-2">
        <button className="btn-primary-outline">
          <label htmlFor="single">
            {uploading
              ? 'Uploading...'
              : avatarUrl
              ? 'Replace avatar'
              : 'Pick avatar'}
          </label>
        </button>
        <input
          accept="image/*"
          className="hidden"
          disabled={uploading}
          id="single"
          onChange={uploadAvatar}
          type="file"
        />
      </div>
    </div>
  )
}
