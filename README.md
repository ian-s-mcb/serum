# serum - Supabase Example for React User Management

An expanded version of the official example from [Supabase][supabase]. Notable
additions include routing via React-Router, a password-based login option, and a
React context for auth props.

### Screencast

![Video screencast of SERUM in action][screencast]

### Steps

1. Clone Supabase repo
1. `cd supabase/docker`
1. `cp .env.example .env`
1. In .env file, set SITE_URL=http://localhost:5173
1. Provision and start container via `docker-compose up`
1. Open Supabase Studio in your browser: `localhost:3000`
1. After clicking on "Default Project", choose "SQL Editor" from left
nav
1. Replace text in textbox with the SQL code shown below
1. Clone this React app and `cd` into it
1. `cp .env.example .env.local`
1. Edit .env.local file. The two lines with empty values should match
the values in the .env file in supabase/docker
1. `npm i`
1. `npm run start`, which should open the app in your browser.

### SQL code for Supabase backend

```sql
-- Wipe
delete from storage.objects;
delete from storage.buckets;
drop table if exists profiles;
delete from auth.users;
drop policy if exists "Avatar images are publicly accessible."
on storage.objects;
drop policy if exists "Anyone can upload an avatar."
on storage.objects;

-- Create a table for Public Profiles
create table profiles (
  id uuid references auth.users not null,
  updated_at timestamp with time zone,
  username text unique,
  avatar_url text,
  website text,
  primary key (id),
  unique(username),
  constraint username_length check (char_length(username) >= 3)
);
alter table profiles enable row level security;
create policy "Public profiles are viewable by everyone."
  on profiles for select
  using ( true );
create policy "Users can insert their own profile."
  on profiles for insert
  with check ( auth.uid() = id );
create policy "Users can update own profile."
  on profiles for update
  using ( auth.uid() = id );
-- Set up Realtime!
begin;
  drop publication if exists supabase_realtime;
  create publication supabase_realtime;
commit;
alter publication supabase_realtime add table profiles;
-- Set up Storage!
insert into storage.buckets (id, name)
values ('avatars', 'avatars');
create policy "Avatar images are publicly accessible."
  on storage.objects for select
  using ( bucket_id = 'avatars' );
create policy "Anyone can upload an avatar."
  on storage.objects for insert
  with check ( bucket_id = 'avatars' );
```

### Acknowledgements

This project used an existing project with a similar aim as a starting
point. It was written by the Supabase Authors and could be found
[here][original-project].

[original-project]: https://github.com/supabase/supabase/tree/master/examples/user-management/react-user-management
[screencast]: https://i.imgur.com/AlZ6Ow5.mp4
[supabase]: https://github.com/supabase/supabase
